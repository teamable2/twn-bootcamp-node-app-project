Project: Building a JavaScript application using webpack and npm as package manager to generate related dependencies for both a Frontend app (React JS) and Backend application like nodejs

Building JS artifact files with "zip" or "TAR file" using npm or yarn package managers. npm and yarn will install dependencies to the application and will not build the dependencies like maven or gradle does with java applications.

In Package.json file, all related dependencies, scripts, metadata of the application will be stored in the package.json file

To install all dependencies for the JS application, we use the command

To build the Frontend & Backend codes, we use "Webpack" which is a dependency also manged by npm.
Webpack will build the JS codes and act as building tool just like maven and gradle tools are to Java applications 

Webpack will bundle both Frontend and Backend codes after a build script is executed by npm (npm build command)

npm install - this command will download all latest dependencies and generate a node_modules folder to host all related depencies. Also a package-lock.json file will be generate to keep the current state of files related to the application

With npm pack command - we create a JS TAR file (.tgz) package folder to store relevant files to the Project. We choose what files are kept in the TAR file. 
